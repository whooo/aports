# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=ncmpc
pkgver=0.37
pkgrel=0
pkgdesc="Ncurses client for the Music Player Daemon (MPD)"
url="https://www.musicpd.org/clients/ncmpc"
arch="all"
license="GPL-2.0-or-later"
makedepends="glib-dev libmpdclient-dev meson ncurses-dev py3-sphinx boost-dev"
subpackages="$pkgname-doc $pkgname-lang $pkgname-dbg"
source="https://www.musicpd.org/download/ncmpc/0/ncmpc-$pkgver.tar.xz
	doc-py3.patch
	"

build() {
	LDFLAGS="$LDFLAGS -lintl" meson \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--buildtype=debugoptimized \
		-Dlirc=disabled \
		-Dcurses=ncurses \
		-Dlyrics_screen=true \
		-Dlyrics_plugin_dir=share/$pkgname/lyrics \
		. output
	ninja -C output
}

check() {
	./output/ncmpc --help > /dev/null
}

package() {
	DESTDIR="$pkgdir" ninja -C output install

	mkdir -p "$pkgdir"/usr/share/$pkgname/lyrics
	install -m755 lyrics/* \
		"$pkgdir"/usr/share/$pkgname/lyrics

	# Remove HTML documentation
	rm -rf "$pkgdir"/usr/share/doc/$pkgname/html
}

sha512sums="0077a0e03eecfb097e4e454d12432abcca3ea8bb8fee6af994afea460d8d42bf95a95af12d25906e4853c3c70104784a496f25bc4c9a99f57ca0e13c7f0ced9e  ncmpc-0.37.tar.xz
de2f4ed40c72047e535b97a7a4ae57d36e15bab167a1724e636628e1a55134c128e1e13d5bc12d3223d095f8d91e966aa660af70004312b58e6d040b8914ba8e  doc-py3.patch"
